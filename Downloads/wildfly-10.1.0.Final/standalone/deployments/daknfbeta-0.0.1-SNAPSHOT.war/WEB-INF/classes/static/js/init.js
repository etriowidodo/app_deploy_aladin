    (function($) {
    $(function() {
        if (typeof(localStorage.userData) == "undefined")
            {
                window.location = window.location.origin+"/daknf/";

            }
            
        
        var userData = JSON.parse(localStorage.userData);
        
   
        var window_height = $(window).height();
        var window_width = $(window).width();
        var $main_container = $('main');
        var $button_collapse = $('.button-collapse');
        var $title = $('.brand-logo');
        var $filter = $('#filter');
        $main_container.css('min-height', window_height - 50);
        $('#splashscreen').delay(1000).fadeOut();
        $('.account span').html(localStorage.username);
    
        var daerah = JSON.parse(localStorage.pemda);
        var level = JSON.parse(localStorage.levelUser);
        var flag_admin = 0;
        if(userData.dataUser[0].kdsatker==null&&userData.dataUser[0].idskpd==null)
        {            
            flag_admin = 'pusat';
            $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> '+level.content[0][1]);
            $('.dinas').remove();
            $('.pemda').parent().remove();
            $('.group-lapreal').remove();
            
        }
        
        if(userData.dataUser[0].kdsatker!=null&&userData.dataUser[0].idskpd==null)
        {
            flag_admin = 'daerah';
            $('.pusat').remove();
            $('.dinas').remove();
            $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> '+level.content[0][1]+' <br><i class="material-icons">language</i> '+daerah.content[0].urpemda);
            
            
        }
        
        if(userData.dataUser[0].kdsatker!=null&&userData.dataUser[0].idskpd!=null)
        {
            flag_admin = 'skpd';
            $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> '+level.content[0][1]+' <br><i class="material-icons">language</i> '+daerah.content[0].urpemda);
            $('.pemda').parent().remove();
            $('#title-lapreal').text('Realisasi Penggunaan');
            $('.pusat').remove();
            $('.link-skpd').remove();
            $('.link-user').remove();
            $('.group-lapreal').remove();
//            $('.group-administrasi').remove();
            
        }
       
        
//         if(userData.dataUser[0].idskpd!=null)
//        {
//            
//            $('.group-daerah').remove();
//            $('.group-user').remove();
//        }
        
//        if(userData.dataUser[0].kdsatker==null||userData.dataUser[0].idskpd!=null)
//        {
//            $('.pemda').parent().remove();
////            $('.group-lapreal').remove();
//        }
        
//         if(userData.dataUser[0].kdsatker==null&&userData.dataUser[0].idskpd==null) // ini super USER admi pusat
//        {
////            $('.pemda').parent().remove();
//            $('.group-lapreal').remove();
////            console.log(userData.dataUser[0].kdsatker);
//        }
       
        if (userData.dataUser[0].kdsatker!=null&&userData.dataUser[0].idskpd==null)
        {
            //MENU DAERAH
//            $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> Admin Daerah');
            function returnMenuRealisasiPenyerapan(selected) {
                                                return $.ajax({
                                                   url : localStorage.base_url+'/getmenulapreal/'+localStorage.thang,
                                                   data: {
                                                           issession: 1,
                                                           selector: selector
                                                       },
                                                   // dataType: "json",
                                                       async: false,
                                                       error: function() {
                                                           alert("Error occured")
                                                       }
                                                 });

                                             }

            var selector    = !0;
            var ajaxObj     = returnMenuRealisasiPenyerapan(selector);
            var laprealMenu = JSON.parse(ajaxObj.responseText); 
            console.log(laprealMenu);
            var liLapreal   = '';
            var liLapgu     = '';
            $.each(laprealMenu.content,function(i,x){
                var idakun   = x[2];
                var nmakunsk = x[4].split(" ");
                var fullname = x[3]+" ( "+nmakunsk[1]+" )";
                
                liLapreal += '<li class="menu"><a data-nmakunsk="'+x[4]+'" data-nama = "'+fullname+'"data-idakun="'+idakun+'" href="#" class="link-lapreal pemda"><i class="material-icons">note_add</i> <span>'+nmakunsk[1]+'</span></a></li>';
                liLapgu   += '<li class="menu"><a data-nmakunsk="'+x[4]+'" data-nama = "'+fullname+'"data-idakun="'+idakun+'" href="#" class="link-lapgu   pusat"><i class="material-icons">note_add</i> <span>'+nmakunsk[1]+'</span></a></li>';
            });
            
            $('#li_lapreal').html(liLapreal);
            $('#li_lapgu').html(liLapgu);
           
//            var dataSaldo = userData.dataUser[0].kdsatker+','+data.content[0].kdprov+','+ data.content[0].kdpemda;
        
        
        
        }

        if (userData.dataUser[0].idskpd!=null)
        {
            //MENU SKPD
//            $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> Admin Daerah');
            function returnMenuRealisasiPenggunaanSkpd(selected) {
                                                return $.ajax({
                                                   url : localStorage.base_url+'/getmenulapguskpd/'+userData.dataUser[0].idskpd,
                                                   data: {
                                                           issession: 1,
                                                           selector: selector
                                                       },
                                                   // dataType: "json",
                                                       async: false,
                                                       error: function() {
                                                           alert("Error occured")
                                                       }
                                                 });

                                             }

            var selector    = !0;
            var ajaxObj     = returnMenuRealisasiPenggunaanSkpd(selector);
            var lapguMenu = JSON.parse(ajaxObj.responseText); 
            console.log(lapguMenu);
            var liLapgu     = '';
            $.each(lapguMenu.content,function(i,x){
                var idakun   = x[2];
                var nmakunsk = x[4].split(" ");
                var fullname = x[3]+" ( "+nmakunsk[1]+" )";
                liLapgu   += '<li class="menu"><a data-nmakunsk="'+x[4]+'" data-nama = "'+fullname+'"data-idakun="'+idakun+'" href="#" class="link-lapgu   pusat"><i class="material-icons">note_add</i> <span>'+nmakunsk[1]+'</span></a></li>';
            });
            
    
            $('#li_lapgu_skpd').html(liLapgu);
           
//            var dataSaldo = userData.dataUser[0].kdsatker+','+data.content[0].kdprov+','+ data.content[0].kdpemda;
        
        
        
        }
//        else if (userData.dataUser[0].idskpd!=null)
//        {
////            $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> Admin SKPD');
//        }
//        else
//        {
////             $('#nickname').html(userData.dataUser[0].full +'<br><i class="material-icons">language</i> Admin Pusat');
//        }
        
       
        
         $('.link-pemda').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('pemda');
            $title.text('Pemda');
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
            $filter.hide();
        });
         $('.link-prov').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('prov');
            $title.text('Provinsi');
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
            $filter.hide();
        });
        $('.link-skpd').click(function() {
            $button_collapse.sideNav('hide');
           if(userData.dataUser[0].kdsatker==null&&userData.dataUser[0].idskpd==null)
            {
                $main_container.load('skpd');
            }
             else if(userData.dataUser[0].idskpd==null)
            {
                $main_container.load('skpd-pemda');
            }
            $title.text('SKPD');
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
            $filter.hide();
        });
        
        $('.link-user').click(function() {
           $button_collapse.sideNav('hide');
           if(userData.dataUser[0].kdsatker==null&&userData.dataUser[0].idskpd==null)
           {
               $main_container.load('user');
           }
            else if(userData.dataUser[0].idskpd==null)
           {
               $main_container.load('user-pemda');
           }
           else
           {
//                    alert(userData.dataUser[0].idskpd);
           }
           $title.text('Pengguna');
           $('.menu').removeClass('bold active');
           $(this).parent().addClass('bold active');
           $filter.hide();
       });
          
        
        
        
        $('.link-daftar-anggota-user').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('transaksi');
            $title.text('Transaksi');            
             $filter.hide();
        });
        
        $('.link-saldo').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('saldo');
            $title.text('Saldo Awal');   
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
             $filter.hide();
        });
        $('.link-pagu').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('pagu');
            $title.text('Pagu');   
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
             $filter.hide();
        });
         $('.link-monitoring').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('monitoring');
            $title.text('Monitoring Pelaporan');   
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
             $filter.hide();
        });
        
         $('.link-monitoring-kepatuhan').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('monitoring-kepatuhan');
            $title.text('Monitoring Kepatuhan');   
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
             $filter.hide();
        });
         
        
        
        $('.link-cetak-laporan-realisasi').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('cetak-lapreal');
            $title.text('Cetak Laporan Realisasi');   
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
             $filter.hide();
        });
        
         $('.link-change-password').click(function() {
            $button_collapse.sideNav('hide');
            $main_container.load('ubah-password');
            $title.text('Ubah Password');   
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
             $filter.hide();
        });
        
          $('.link-logout').click(function() {            
            localStorage.clear();
            window.location.href = "login.html";
        });

        // $(document).ajaxStart(function() {
        //     $('#loading').show();
        // });
        // $(document).ajaxStop(function() {
        //     $('#loading').hide();
        // });
        // convert rgb to hex value string
        function rgb2hex(rgb) {
            if (/^#[0-9A-F]{6}$/i.test(rgb)) {
                return rgb;
            }

            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

            if (rgb === null) {
                return "N/A";
            }

            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }

            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }

        $('.dynamic-color .col').each(function() {
            $(this).children().each(function() {
                var color = $(this).css('background-color'),
                    classes = $(this).attr('class');
                $(this).html(rgb2hex(color) + " " + classes);
                if (classes.indexOf("darken") >= 0 || $(this).hasClass('black')) {
                    $(this).css('color', 'rgba(255,255,255,.9');
                }
            });
        });
        
        // Detect touch screen and enable scrollbar if necessary
        function is_touch_device() {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        }
        if (is_touch_device()) {
            $('#nav-mobile').css({
                overflow: 'auto'
            });
        }

        // Set checkbox on forms.html to indeterminate
        var indeterminateCheckbox = document.getElementById('indeterminate-checkbox');
        if (indeterminateCheckbox !== null)
            indeterminateCheckbox.indeterminate = true;

        // Plugin initialization
        $button_collapse.sideNav({
            edge: 'left'
        });
        $('.datepicker').pickadate({
            selectYears: 20
        });
        $('select').not('.disabled').material_select();
        
        $(document).on({

        ajaxStart: function() { $('#splashscreen').show();  },
        ajaxSuccess: function() { $('#splashscreen').hide(); },
        submit  : function(){ $('#splashscreen').show();}


        });

        $(document).on("click", ".link-lapreal", function (e) {
            $button_collapse.sideNav('hide');
            localStorage.laprealIdAkun = $(this).attr('data-idakun');
            localStorage.nmakunSk = $(this).attr('data-nama');
            localStorage.nmakunSk2 = $(this).attr('data-nmakunsk');
            $title.text('Laporan Realisasi Penyerapan Angaran'+localStorage.nmakunSk2);
    
            
           if(localStorage.laprealIdAkun=='654311'){
                 $main_container.load('lapreal-bos'); 
            }
            else{
                 $main_container.load('lapreal');  
            }
            
            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
            $filter.hide();
            e.preventDefault();
        });
        $(document).on("click", ".link-lapgu", function (e) {
            $button_collapse.sideNav('hide');
            localStorage.laprealIdAkun = $(this).attr('data-idakun');
            localStorage.nmakunSk = $(this).attr('data-nama');
            localStorage.nmakunSk2 = $(this).attr('data-nmakunsk');
            

            
            $title.text('Laporan Realisasi Penggunaan Angaran'+localStorage.nmakunSk2);

            $('.menu').removeClass('bold active');
            $(this).parent().addClass('bold active');
            $filter.hide();
            e.preventDefault();
            if(localStorage.laprealIdAkun=='654811'){
                 $main_container.load('lapgu-pk2ukm'); 
            }
            else{
              $main_container.load('lapreal-skpd');  
            }
            
        });
//            $.ajaxSetup({
//                async : false,
//                chache : false
//            })
    }); // end of document ready
    
    
})(jQuery); // end of jQuery name space
