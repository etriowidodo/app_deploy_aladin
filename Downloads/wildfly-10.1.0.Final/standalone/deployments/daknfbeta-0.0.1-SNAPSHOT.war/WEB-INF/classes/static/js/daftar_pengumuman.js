// function get_user_by_zona(id)
//             {
//                 var dataString = "zona="+id;
//                 $.ajax({
//                     type: "POST",
//                     url: localStorage.url+"get_user_by_zona.php",
//                     data: dataString,
//                     crossDomain: true,
//                     dataType: 'json',
//                     cache: false,
//                 success: function(result) {
//                     var data = jQuery.parseJSON(JSON.stringify(result));
//                     var html = '';
//                     var li   = '';
//                     $('.collection-item').remove();
//                      $.each(result,function(i,item)
//                      {
//                           li    = '<li class="collection-item"><div>'+item.nama+'<a href="javascript:void(0)" onclick="edit_pengumuman('+item.id_user+')"class="secondary-content"><i class="material-icons">pageview</i></a><a href="javascript:void(0)" onclick="delete_pengumuman('+item.id_user+')"class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
//                           $('.with-header').append(li);
//                          });
//                         }
//                     });
//             }

// //$('#zona').material_select();
function edit_pengumuman(id)
{
     

    $('#filter').hide();
    var dataString = "id_pengumuman="+id;
    $.ajax({
                type: "POST",
                url: localStorage.url+"get_pengumuman_by_id.php",
                data: dataString,
                crossDomain: true,
                dataType: 'json',
                cache: false,
                success: function(result) {
                    $('#edit').removeClass('hide');
                    $('#grid').addClass('hide');
                    var data1 = jQuery.parseJSON(JSON.stringify(result));

                    // select_zona(data[0].zona);
                    $('#judul_pengumuman').val(data1[0].judul_pengumuman);
                    $('#pengumuman').val(data1[0].pengumuman);
                    $('#id_pengumuman').val(data1[0].id_pengumuman);
                    $.ajax({
                              type: "POST",
                              url: localStorage.url+"ajax_zona.php",
                              crossDomain: true,
                              dataType: 'json',
                              cache: false,
                              success: function(result) {
                              var data = jQuery.parseJSON(JSON.stringify(result));
                              var html = '';
                              var li   = '';
                              $.each(result,function(i,item)
                               {
                                if(item.id_zona!=8)
                                {
                                //  li    = "<li  ><a href='javascript:void(0)' onclick=get_user_by_zona('"+item.id_zona+"')>"+item.zona+"</a></li>";
                                  li    =     '<p class="collection-item"> '+
                                                  '<input class="filled-in" value='+item.id_zona+' type="checkbox" id="'+item.id_zona+'" >'+
                                                  '<label class="orange-text " for="'+item.id_zona+'">'+item.zona+'</label>'+
                                              '</p>';
                                  if($('div.collection p').length==7)
                                  {
                                    $('div.collection p').remove();
                                    $('div.collection').append(li);
                                  }else
                                  {
                                    $('div.collection').append(li);
                                  }
                                  
                                }
                               });

                               $.each(data1,function(i,item)
                               {
                                 $('input#'+item.zona).prop('checked',true);
                               });
                               
                              }
                          });
                }

            });
}

function delete_pengumuman(id)
{
    if(confirm('anda yakin akan menghapus data ini ?'))
    {
    var dataString = "id_pengumuman="+id;
    $.ajax({
                type: "POST",
                url: localStorage.url+"delete_pengumuman_by_id.php",
                data: dataString,
                crossDomain: true,
                cache: false,
                success: function(result) {
                    
                    var data = jQuery.parseJSON(result);
                    if (data.success == 1) {
                        Materialize.toast("pengumuman berhasil dihapus", 3000);
                        $('.link-daftar-pengumuman').click();
                    } else if (data.success == 0) {
                        alert("Gagal menambahkan pengumuman, cek koneksi internet Anda.");
                    } 
                }

            });
    }
}


