function get_user_by_zona(id)
            {
              if($('h4').text()=='Anggota')
              {
                var dataString = "zona="+id;
                $.ajax({
                    type: "POST",
                    url: localStorage.url+"get_user_by_zona.php",
                    data: dataString,
                    crossDomain: true,
                    dataType: 'json',
                    cache: false,
                success: function(result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    var html = '';
                    var li   = '';
                    $('.collection-item').remove();
                     $.each(result,function(i,item)
                     {
                          li    = '<li class="collection-item"><div>'+item.nama+'<a href="javascript:void(0)" onclick="edit_anggota('+item.id_user+')"class="secondary-content"><i class="material-icons">pageview</i></a><a href="javascript:void(0)" onclick="delete_anggota('+item.id_user+')"class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
                          $('.with-header').append(li);
                         });
                        }
                    });
              } 
              else if ($('h4').text()=='Pengumuman')
              {
                  var dataString = "zona="+id;
                  $.ajax({
                      type: "POST",
                      url: localStorage.url+"get_pengumuman_by_zona.php",
                      data: dataString,
                      crossDomain: true,
                      dataType: 'json',
                      cache: false,
                  success: function(result) {
                      var data = jQuery.parseJSON(JSON.stringify(result));
                      var html = '';
                      var li   = '';
                      $('.collection-item').remove();
                       $.each(result,function(i,item)
                       {
                            li    = '<li class="collection-item"><div>'+item.judul_pengumuman+'<a href="javascript:void(0)" onclick="edit_pengumuman('+item.id_pengumuman+')"class="secondary-content"><i class="material-icons">pageview</i></a><a href="javascript:void(0)" onclick="delete_pengumuman('+item.id_pengumuman+')"class="secondary-content"><i class="material-icons">delete</i></a></div></li>';
                            $('ul.with-header').append(li);
                           });
                          }
                      });
              }

                
            }

//$('#zona').material_select();
function edit_anggota(id)
{
    $('#filter').hide();
    var dataString = "id_user="+id;
    $.ajax({
                type: "POST",
                url: localStorage.url+"get_user_by_id.php",
                data: dataString,
                crossDomain: true,
                cache: false,
                success: function(result) {
                    $('#edit').removeClass('hide');
                    $('#grid').addClass('hide');
                    var data = jQuery.parseJSON(result);
                    select_zona(data[0].zona);
                    $('#zona_old').val(data[0].zona);
                    $('#id_user').val(data[0].id_user);
                    $('#nama').val(data[0].nama);
                    $('#tempat_lahir').val(data[0].tempat_lahir);
                    $('#tgl_lahir').val(data[0].tgl_lahir);
                    $('#no_telp').val(data[0].no_telp);
                    $('#alamat').val(data[0].alamat);
                    $('#asal_sekolah').val(data[0].asal_sekolah);
                    $('#username').val(data[0].username);
                    $('#password').val(data[0].password);
                    select_zona(data[0].zona);
                }

            });
}

function delete_anggota(id)
{
    if(confirm('anda yakin akan menghapus data ini ?'))
    {
    var dataString = "id_user="+id;
    $.ajax({
                type: "POST",
                url: localStorage.url+"delete_user_by_id.php",
                data: dataString,
                crossDomain: true,
                cache: false,
                success: function(result) {
                    $('#edit').removeClass('hide');
                    $('#grid').addClass('hide');
                    var data = jQuery.parseJSON(result);
                    if (data.success == 1) {
                        Materialize.toast("Anggota berhasil dihapus", 3000);
                        $('.link-daftar-anggota').click();
                    } else if (data.success == 0) {
                        alert("Gagal menambahkan anggota, cek koneksi internet Anda.");
                    } 
                }

            });
    }
}
function select_zona(id)
{
    $.ajax({
                type: "POST",
                url: localStorage.url+"ajax_zona.php",
                crossDomain: true,
                dataType: 'json',
                cache: false,
                success: function(result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                var html = '';
                var li   = '';
                 $.each(result,function(i,item)
                 {
                  html  = "<option value='"+item.id_zona+"'>-- "+item.zona+" --</option>";
                  li    = "<li class=''><span>"+item.zona+"</span></li>";
                  if(item.id_zona==id)
                  {
                    $('select').html(html);
                    $('.select-dropdown').html(li);
                  }
                 });
                  $.each(result,function(i,item)
                 {
                  html  = "<option value='"+item.id_zona+"'>-- "+item.zona+" --</option>";
                  li    = "<li class=''><span>"+item.zona+"</span></li>";
                  if(item.id_zona!=id)
                  {
                    $('select').append(html);
                    $('.select-dropdown').append(li);
                  }
                  $('#zona').material_select();
                 });
                   

                }
            });
      
}