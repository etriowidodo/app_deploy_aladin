(function($) {
    $(function() {
        
if (typeof(localStorage.userData) != "undefined")
{    
   window.location = localStorage.base_url+'/home';
    
}


        var window_width = $(window).width();
        $('#splashscreen').delay(1000).fadeOut();

        $(document).ajaxStart(function() {
            $('#loading').show();
        });
        $(document).ajaxStop(function() {
            $('#loading').hide();
        });
        
        $( document ).ajaxError(function() {
            $('#loading').hide();
          });

        $("#login_button").click(function() {
            
            var url = window.location.origin+"/daknf/login";
            var username    = $("#username").val();
            var password    = $("#password").val();
            var tahun       = $("#thang").val();
;            var data        = JSON.stringify({'nmuser':username,'pass':password});            
            if (username !='' && password != '' && tahun !='') {
                $.ajax({
                    type: "POST",
                    url: url,
                    headers:{'Accept':'application/json','Content-Type':'application/json'},
                    data: data,
                    beforeSend: function() {
                        $("#login_button").html('Processing...');
                        $("#login_button").off('click');
                    },
                    success: function(result) {
                      if(result.count == 1)
                      {
                         var data = JSON.stringify({'dataUser':result.dataUser});
                         localStorage.userData  = data;
                         localStorage.base_url  = window.location.origin+"/daknf";
                         var tahun = new Date();
                         var tahun = tahun.getFullYear();
                         localStorage.thang     = tahun;
                         var data = JSON.parse(data);
//                         console.log(data.dataUser[0].kdsatker);
                          function returnTpemda(selected) {
                                                return $.ajax({
                                                   url : localStorage.base_url+'/getpemdabykdsatker/'+data.dataUser[0].kdsatker,
                                                   data: {
                                                           issession: 1,
                                                           selector: selector
                                                       },
                                                   // dataType: "json",
                                                       async: false,
                                                       error: function() {
                                                           alert("Error occured")
                                                       }
                                                 });

                                             }

                            var selector    = !0;
                            var ajaxObj     = returnTpemda(selector);
                            var tPemda      = JSON.parse(ajaxObj.responseText); 
                            
                            function returnLevel(selected) {
                                                return $.ajax({
                                                   url : localStorage.base_url+'/getleveluser/'+data.dataUser[0].idlevel,
                                                   data: {
                                                           issession: 1,
                                                           selector: selector
                                                       },
                                                   // dataType: "json",
                                                       async: false,
                                                       error: function() {
                                                           alert("Error occured")
                                                       }
                                                 });

                                             }

                            var selector    = !0;
                            var ajaxObj     = returnLevel(selector);
                            var tLevel      = JSON.parse(ajaxObj.responseText); 
                          localStorage.levelUser = JSON.stringify(tLevel);
                          localStorage.pemda = JSON.stringify(tPemda);
                          $("#login_button").html('Login');  
                          $("#login_button").on('click');
                           window.location = localStorage.base_url+'/home';
                         
                      }
                      else 
                      {
                            $("#login_button").html('Login');                            
                            $("#login_button").on();
                            Materialize.toast('User Dan Password Tidak Ditemukan', 1000);
                            setTimeout(function(){ location.reload(); }, 1000); 
                      }
                    }
                });
            }
            else
            {
                Materialize.toast('Isi Form Dengan Benar', 1000);
            }
            return false;
        });

        // Detect touch screen and enable scrollbar if necessary
        function is_touch_device() {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        }
        if (is_touch_device()) {
            $('#nav-mobile').css({
                overflow: 'auto'
            });
        }
        
        
        $('#username,#password').keypress(function (e) {
            if (e.which == 13) {
              $('#login_button').click();
              return false;    //<---- Add this line
            }
          });

    }); // end of document ready
})(jQuery); // end of jQuery name space
